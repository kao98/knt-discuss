module ApplicationHelper
  
  def activate_menu(menu)
    'active' if 
      (menu == 'profile'   && params[:controller] == 'users') ||
      (menu == 'dashboard' && params[:controller] != 'users')
  end

end
