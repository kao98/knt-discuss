module DashboardHelper

    def activate_current_topic(topic, current_topic)
        'email-item-current' if topic.id == current_topic.id
    end

end
