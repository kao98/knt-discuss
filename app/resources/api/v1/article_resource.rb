class Api::V1::ArticleResource < JSONAPI::Resource
  attributes :name, :url, :messages
end
