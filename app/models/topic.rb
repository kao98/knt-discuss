class Topic < ActiveRecord::Base
    has_many :posts, dependent: :delete_all
    belongs_to :user
end
