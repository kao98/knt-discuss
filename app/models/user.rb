class User < ActiveRecord::Base
  before_save { self.email = email.downcase }

  has_secure_password
  validates :password, presence: true, length: { minimum: 8 }, confirmation: true, on: :create

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, length: { maximum: 255 },
    format: { with: VALID_EMAIL_REGEX },
    uniqueness: { case_sensitive: false }

  has_many :topics, dependent: :nullify
  has_many :authors, dependent: :nullify

  def display_username
    if username.nil?
      email.gsub(/\@.*/, '')
    else
      username
    end
  end
  alias_method :name, :display_username

end
