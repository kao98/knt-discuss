class Author < ActiveRecord::Base
    has_many :posts, dependent: :nullify
    belongs_to :user
end
