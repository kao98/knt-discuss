class SessionController < ApplicationController
  layout "landing" 
  def new
  end

  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      log_in user
      redirect_to dashboard_index_url
    else
      flash.now[:error] = '<h3>Invalid email/password</h3><p>Please verify your credentials</p>'
      render 'new'
    end
  end

  def destroy
    log_out
    redirect_to root_url
  end

end
