class WelcomeController < ApplicationController
  layout "landing"

  def index
    if logged_in
      redirect_to dashboard_index_url
    end
  end
end
