class UsersController < ApplicationController
  layout "application", :except => [:signup, :create]
  layout "landing", :only => [:signup, :create]

  def signup
  end

  def create
    @user = User.new(signup_params)
    if @user.save
      log_in @user
      flash[:success] = "Welcome to Knt-Discuss! :-)"
      redirect_to dashboard_index_url
    else
      flash.now[:error] = "Something's wrong, please verify your submission"
      render 'signup'
    end
  end

  def show
  end

  private

    def signup_params
      params.require(:user).permit(:email, :password, :password_confirmation)
    end
end
