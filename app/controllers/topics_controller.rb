class TopicsController < ApplicationController
  def new
  end

  def create
    params[:topic].permit!
    @topic = Topic.new(params[:topic])
    @topic.user = current_user
    @topic.save!
    redirect_to controller: :dashboard, action: :index, topic_id: @topic.id
  end
end
