class DashboardController < ApplicationController
  before_action :logged_in_user

  def index
    @user = current_user
    @current_topic = find_current_topic
  end

  def new_discussion
    @user = current_user
    @current_topic = Topic.new

    render :index
  end

  private

  def find_current_topic

    unless params[:topic_id].nil?
      current_user.topics.find(params[:topic_id])
    else
      current_user.topics.first
    end

  rescue
    current_user.topics.first
  end

end
