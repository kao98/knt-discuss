Rails.application.routes.draw do

  # root / landing page / dashboard
  root 'welcome#index'
  get 'welcome/index'
  get 'dashboard', to: redirect('dashboard/index')
  get 'dashboard/index'
  get 'dashboard/new_discussion'


  # session
  get 'login' => 'session#new'
  post 'login' => 'session#create'
  get 'logout' => 'session#destroy'

  # other resources

  resources :users
  get 'signup' => 'users#signup'
  post 'signup' => 'users#create'

  resources :topics, :only => [:new, :create]

  namespace :api do
    namespace :v1 do
      jsonapi_resources :articles
    end
  end

end
