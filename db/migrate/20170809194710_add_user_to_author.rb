class AddUserToAuthor < ActiveRecord::Migration[4.2]
  def change
    add_reference :authors, :user, foreign_key: true, index: true
  end
end
