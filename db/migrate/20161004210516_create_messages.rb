class CreateMessages < ActiveRecord::Migration[4.2]
  def change
    create_table :messages do |t|
      t.text :body
      t.references :author, foreigh_key: true

      t.timestamps
    end
  end
end
