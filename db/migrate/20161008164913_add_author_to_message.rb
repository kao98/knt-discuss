class AddAuthorToMessage < ActiveRecord::Migration[4.2]
  def change
    remove_column :messages, :author_id
    add_reference :messages, :author, foreign_key: true, index: true
  end
end
