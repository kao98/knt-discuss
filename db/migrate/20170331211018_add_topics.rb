class AddTopics < ActiveRecord::Migration[4.2]
  def change
    rename_column :articles, :name, :title
    rename_column :messages, :article_id, :topic_id
    rename_table :articles, :topics
    rename_table :messages, :posts

    add_column :topics, :host, :string
    add_reference :topics, :user, foreign_key: true, index: true
  end
end
