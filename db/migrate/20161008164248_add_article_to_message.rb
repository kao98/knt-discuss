class AddArticleToMessage < ActiveRecord::Migration[4.2]
  def change
    add_reference :messages, :article, index: true, foreign_key: true
  end
end
