require 'test_helper'

class ArticlesControllerTest < ActionController::TestCase
  test "index list articles" do
    get :index, headers: {"Content-Type" => "application/vnd.api+json", "Accept" => "application/vnd.api+json"}
    exp = {name: ''}
    assert_equal exp, JSON.parse(@response.body)
  end

end
