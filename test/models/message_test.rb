require 'test_helper'

class MessageTest < ActiveSupport::TestCase
  test "Message article 2" do
    message_three = messages(:three)
    assert_equal message_three.author, authors(:sc00p)
  end
end
